Logging
-------

Log changes, showing statistics for each file:::

   git log --stat

Log changes, showing differences for each file:::

   git log --patch

Log the last 10 changes to a file, on all branches:::

   git log -n 10 --all --follow --stat -- dir/file

Show the last 10 changes to a file, on all branches:::

   git log -n 10 --all --follow --patch -- dir/file


.. toctree::
   :maxdepth: 4
